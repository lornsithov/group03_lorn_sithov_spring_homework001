package com.spring_homework001.group03_lorn_sithov_spring_homework001.controller;

import com.spring_homework001.group03_lorn_sithov_spring_homework001.Customer;
import com.spring_homework001.group03_lorn_sithov_spring_homework001.CustomerRequest;
import com.spring_homework001.group03_lorn_sithov_spring_homework001.CustomerResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1/customers/")
public class customersController {
    ArrayList<Customer> customers = new ArrayList<>();

    Customer c = new Customer();

    public customersController() {
        customers.add(new Customer(1, 20, "Juice", "Male", "NY"));
        customers.add(new Customer(2, 23, "Mac", "Male", "LA"));
        customers.add(new Customer(3, 24, "Vannda", "Male", "KPS"));
    }


    // get all customers
    @GetMapping("/")
    public ResponseEntity<CustomerResponse<ArrayList<Customer>>> getAllCustomers() {
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
            "Successfully getting all customers!",
            customers,
            HttpStatus.OK,
            LocalDateTime.now()
        ));
    }

    // insert customer
    @PostMapping("/")
    public ResponseEntity<CustomerResponse<Customer>> insertCustomers(@RequestBody CustomerRequest customer) {
        c.setId(customers.size() + 1);
        c.setName(customer.getName());
        c.setAddress(customer.getAddress());
        c.setAge(customer.getAge());
        c.setGender(customer.getGender());
        customers.add(c);
//        return c;
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                "This record was successfully created",
                c,
                HttpStatus.OK,
                LocalDateTime.now()
        ));
    }

    // get customer by ID
    @GetMapping("/{customerId}")
    public  ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable Integer customerId) {
        for (Customer cus : customers) {
            if (cus.getId() == customerId){
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                    "This record has been found successfully",
                    cus,
                    HttpStatus.OK,
                    LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

    //delete customer by id
    @DeleteMapping("/{customerId}")
    public ResponseEntity<CustomerResponse<Customer>> deleteCustomerById(@PathVariable Integer customerId) {
        for (Customer cus : customers) {
            if (cus.getId() == customerId){
                customers.remove(cus);
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                    "This record has been deleted successfully",
                    null,
                    HttpStatus.OK,
                    LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

    //update customer by id
    @PutMapping("/{customerId}")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@RequestBody CustomerRequest c,@PathVariable Integer customerId) {
        Customer cusNull = new Customer();
        cusNull = null;
        for (Customer cus : customers) {
            if (cus.getId() == customerId){
                cus.setAge(c.getAge());
                cus.setName(c.getName());
                cus.setGender(c.getGender());
                cus.setAddress(c.getAddress());
                cusNull = new Customer(cus.getId(), c.getAge(), c.getName(), c.getGender(), c.getAddress());
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                    "This record has been updated successfully",
                    cusNull,
                    HttpStatus.OK,
                    LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

    // find customer by name
    @GetMapping("/search")
    public ResponseEntity<CustomerResponse<Customer>> findCustomerByName(@RequestParam String name) {
        for (Customer cus : customers) {
            if (cus.getName().equals(name)) {
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                    "This record has been found successfully",
                    cus,
                    HttpStatus.OK,
                    LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

}
