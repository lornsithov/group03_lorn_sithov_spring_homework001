package com.spring_homework001.group03_lorn_sithov_spring_homework001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Group03LornSithovSpringHomework001Application {

    public static void main(String[] args) {
        SpringApplication.run(Group03LornSithovSpringHomework001Application.class, args);
    }

}
