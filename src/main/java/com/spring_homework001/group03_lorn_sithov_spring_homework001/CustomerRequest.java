package com.spring_homework001.group03_lorn_sithov_spring_homework001;

public class CustomerRequest {
    private int age;
    private String name, gender, address;

    public CustomerRequest(int age, String name, String gender, String address) {
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.address = address;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }
}
