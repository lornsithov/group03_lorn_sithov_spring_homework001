package com.spring_homework001.group03_lorn_sithov_spring_homework001;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class CustomerResponse <T>{
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T customer;
    private HttpStatus status;
    private LocalDateTime dateTime;

    public CustomerResponse(String message, T customer, HttpStatus status, LocalDateTime dateTime) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.dateTime = dateTime;
    }

    public String getMessage() {
        return message;
    }

    public T getCustomer() {
        return customer;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
